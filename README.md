moviecat
========

Python script to generate the corresponding ffmpeg filters to
concatenate multiple video clips.

Please note:
 * This script does not do the concatenation, it only provides
   the necessary ffmpeg -filter_complex pipeline definition
   for a proper concatenation. FFmpeg must still be called
   appropriately:
   ```
      ffmpeg -i file1.mp4 -i file2.mp4 \
      -filter_complex $(./moviecat.py3 ...) \
      -o output.mp4
   ```

 * Sometimes ffmpeg crashes when using the crossfade transition.
   Try varying the crossfade transition for 0.1 seconds.

 * Clips must fulfill requirements by the ffmpeg concat filter:
 
   * All segments must have the same number of streams of each
     type, and that will also be the number of streams at output.
     
   * All corresponding streams must have the same parameters in
     all segments.

 * This script has bugs... many bugs... loads of bugs. Please
   report them.

 * ffprobe is used to get media file info. Make sure it's
   installed.

How it works
------------

1. It interprets input from the command-line arguments. For example:
   ``-i fade 2 -f day.mp4 -c fade 2 -f night.mp4 -o fade 2``
   
   This will mean that the user is asking to concatenate ``day.mp4`` (suppose
   it is a 10-second clip) and ``night.mp4`` (suppose it is a 20-second clip)
   with an initial fade-in from black, a cross-fade between the two media
   clips and an ending fade-out to black.

2. This is interpreted as "steps", which is just the result of parsing the
   arguments.

3. Then, moviecat will convert the steps into "segs" (segments), which are
   the abstract representation after logic is applied of what would be
   the output. For the previous example:
   * 2 seconds of a fade-in (fade from black) to day.mp4.
   * 6 seconds of plain ``day.mp4`` starting on second 2.
   * 2 seconds of a cross-fading day.mp4 from second 8 and night.mp4.
   * 16 seconds of ``night.mp4`` starting on second 2.
   * 2 seconds of a fade-out (fade to black) from night.mp4 starting on
     second 8.
   
4. Once the "segs" are calculated, they are converted into ``-filter_complex``
   representation.

