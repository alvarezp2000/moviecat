#!/usr/bin/env python3

#Sample idea of the command-line call
#moviecat \
#  --reference red.mp4
#  -transition-in fade 1 \
#  red.mp4 \
#  -transition-cross crossfade 2 \
#  green.mp4
#  -transition-out fade 3 \
#  -transition-in fade 4 \
#  blue.mp4
#  magenta.mp4
#  -transition-out fade 5
#  -o fade2-black-to-red.mp4 <------ we don't do this yet, only filter output

# https://stackoverflow.com/questions/5574702/how-to-print-to-stderr-in-python
from __future__ import print_function
import sys

def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)

SEMICOLON = ";"

import sys
import os
import json
import subprocess

def eprint_help():
    helptext = """
moviecat: concatenates video files

usage: moviecat [step ...]

A step may be a file:
    -file filename
    
or a transition:
    -in fade length
    -out fade length
    -cross fade length

For now there are some limitations:

 * moviecat does not calls ffmpeg, it justs outputs the
   needed -filter_complex for ffmpeg. So, to use it, call ffmpeg
   like this:
     ffmpeg -i 1.mp4 -i 2.mp4 \\
       -filter_complex "$(moviecat -file 1.mp4 -cross fade 1 -file 2.mp4)" \\
       -map '[vout]' -map '[aout]' -o output.mp4

 * Input videos must be of the same type: only concatenate videos
   captured with the same camera or recording device.
"""
    
    eprint(helptext)


# https://stackoverflow.com/questions/354038/how-do-i-check-if-a-string-is-a-number-float
def is_float(s):
    try:
        float(s)
        return True
    except ValueError:
        return False

# Combination of techniques taken from:
# https://dbader.org/blog/python-check-if-file-exists
def is_readable_file(path):
    if not os.path.isfile(path):
        return False
    try:
        f = open(path, 'r')
        f.close()
    except:
        return False
    return True

def validate_transition(f, available_transitions):
    errors = list()
    if f[1] not in available_transitions:
        errors.append(f'In "{" ".join(f)}", invalid {f[0]} transition type: {f[1]}')
    if not is_float(f[2]):
        errors.append(f'In "{" ".join(f)}", invalid {f[0]} transition length: {f[2]}')
    return errors

def validate_file(f):
    errors = list()
    if not is_readable_file(f[1]):
        errors.append(f'In "{" ".join(f)}", file is unreadable or not a file: {f[1]}')
    return errors

def validate_steps(steps):
    errors = list()
    for s in steps:
        if s[0] in ['-in', '-i']:
            errors.extend(validate_transition(s, ['fade']))
        elif s[0] in ['-out', '-o']:
            errors.extend(validate_transition(s, ['fade']))
        elif s[0] in ['-cross', '-c']:
            errors.extend(validate_transition(s, ['fade']))
        elif s[0] in ['-file', '-f']:
            errors.extend(validate_file(s))
        else:
            errors.append(f'In "{" ".join(s)}", invalid section: {s[0]}')
    return errors

def validate_profile(r, steps):
    errors = list()
    sources = [s[1] for s in steps if s[0] == '-file']
    if not r[0] in sources:
        errors.append(f'In "{" ".join(r)}", file is unreadable or not a file: {r[1]}')
    return errors

def validate_settings(settings, steps):
    errors = list()
    for s in settings:
        if s in ['profile']:
            errors.extend(validate_profile(settings[s], steps))
        else:
            errors.append(f'In "{" ".join([f"--{s}"] + settings[s])}", invalid setting: --{s}')
    return errors


## CONVERT THE STEPS INTO FULLY-DETAILED "SEG(MENT)S"

def ffprobe(file):
    ffprobe_call = "ffprobe -v error -show_format -show_streams -of json".split(" ")
    ffprobe_call.append(file)
    return json.loads(subprocess.check_output(ffprobe_call))

def calculate_seg_in(n, steps, file_n, segs):
    input = file_n + 1
    length = float(steps[n][2])
    output = f't{input}i'
    return {'type': steps[n][1], 'length': length, 'input': input, 'output': output}

def calculate_seg_out(n, steps, file_n, segs):
    input = file_n
    length = float(steps[n][2])
    start = segs[n - 1][1]['file_length'] - length
    output = f't{input}o'
    return {'type': steps[n][1], 'start': start, 'length': length, 'input': input, \
    'output': output}

def calculate_seg_cross(n, steps, file_n, segs):
    inputold = file_n
    inputnew = file_n + 1
    length = float(steps[n][2])
    inputold_start = segs[n - 1][1]['file_length'] - length
    output = f't{inputold}{inputnew}c'
    return {'type': steps[n][1], 'length': length, \
        'inputold': inputold, 'inputnew': inputnew, \
        'inputold_start': inputold_start, 'output': output}
    
def calculate_seg_inside(n, steps, file_n, segs):
    input = file_n
    start = 0
    if n != 0 and segs[n - 1][0] in ['in', 'cross']:
        start = segs[n - 1][1]['length']
    file_length = float(ffprobe(steps[n][1])['format']['duration'])
    length = file_length - start
    if len(steps) > n + 1 and steps[n + 1][0] in ['-o', '-out', '-c', '-cross']:
        length = length - float(steps[n + 1][2])
    output = f's{input}'
    return {'name': steps[n][1], 'start': start, \
        'length': length, 'input': input, 'output': output, \
        'file_length': file_length}

def validate_seg_order(segs):
    errors = list()
    for n in range(len(segs)):
        if n == 0:
            if segs[n][0] not in ['in', 'inside']:
                errors.append('The first segment must be either -in or -file')
        elif n == len(segs) - 1:
            if segs[n][0] not in ['out', 'inside']:
                errors.append('The last segment must be either -out or -file')
        elif segs[n][0] == 'in' and segs[n-1][0] not in ['out', 'file']:
            errors.append('An -in segment can only be preceded by an -out or a -file')
        elif segs[n][0] == 'in' and segs[n+1][0] not in ['inside']:
            errors.append('An -in segment can only be followed by an -out or a -file')
        elif segs[n][0] == 'out' and segs[n-1][0] not in ['inside']:
            errors.append('An -out segment can only be preceded by a -file')
        elif segs[n][0] == 'out' and segs[n+1][0] not in ['in', 'inside']:
            errors.append('An -out segment can only be followed by an -in or a -file')
        elif segs[n][0] == 'cross' and segs[n-1][0] not in ['inside']:
            errors.append('A -cross segment can only be preceded by a -file')
        elif segs[n][0] == 'cross' and segs[n+1][0] not in ['inside']:
            errors.append('A -cross segment can only be followed by a -file')
    return errors                
        


#Sample idea of what segs is:
#segs = [
#    ['in',        {type: 'fade', length: 1, input: '0', output: 'v0t0i'}],
#    ['inside',    {name: '2020-06-03-002507.webm', start: 1, length: 4.175, input: '0', output: 'v0s0i'}],
#    ['cross',     {type: 'fade', length: 3, inputold: '0', inputnew: '1', inputold_start: 5.175, output: 'v0t01c'}],
#    ['inside',    {name: '2020-06-03-002528.webm', start: 3, length: 4.674, input: '1', output: 'v0s1i'}],
#    ['out',       {type: 'fade', length: 1, start: 7.674, input: '1', output: 'v0t1o'}],
#]


### INTRO_IN
def fade_in(input, length, output):
    vfade = f"[v{input}]fade=d={length}[v{output}]"
    afade = f"[a{input}]afade=d={length}:curve=tri[a{output}]"
    return SEMICOLON.join([vfade, afade])

def t_in(input, length, type, output):
    if type == "fade":
      return fade_in(input, length, output)
    eprint("t_in: something went wrong")

# SECTION
def seg_trans_in(input, length, type, output):
    vprepare_filter = f"[{input}:v]trim=end={length},setpts=PTS-STARTPTS[v{output}p]"
    aprepare_filter = f"[{input}:a]aformat=s32p,atrim=end={length},asetpts=PTS-STARTPTS[a{output}p]"
    transition_filter = t_in(f"{output}p", length, type, output)
    return SEMICOLON.join([vprepare_filter, aprepare_filter, transition_filter])


### INSIDE
# SECTION
def seg_inside(input, start, length, output):
    vinside = f"[{input}:v]trim=start={start}:duration={length},setpts=PTS-STARTPTS[v{output}]"
    ainside = f"[{input}:a]aformat=s32p,atrim=start={start}:duration={length},asetpts=PTS-STARTPTS[a{output}]"
    return SEMICOLON.join([vinside, ainside])


### TRANSITION: CROSS
def cross_fade(inputold, inputnew, length, output):
    #Using FIFO per recommendation at: https://superuser.com/questions/1001039/what-is-an-efficient-way-to-do-a-video-crossfade-with-ffmpeg
    vprepareold = f"[v{inputold}]format=yuva420p,fade=type=out:duration={length}:alpha=1,fifo[v{inputold}af]"
    vpreparenew = f"[v{inputnew}]fifo[v{inputnew}f]"
    voverlay = f"[v{inputnew}f][v{inputold}af]overlay[v{output}]"
    acrossfade = f"[a{inputold}][a{inputnew}]acrossfade=d={length}[a{output}]"
    return SEMICOLON.join([vprepareold, vpreparenew, voverlay, acrossfade])

def t_cross(inputold, inputnew, length, type, output):
    if type == "fade":
       return cross_fade(inputold, inputnew, length, output)
    eprint("t_cross: something went wrong")

# SECTION
def seg_trans_cross(inputold, inputnew, inputold_start, length, type, output):
    vprepare_filter_old = f"[{inputold}:v]trim=start={inputold_start},setpts=PTS-STARTPTS[v{output}s0p]"
    aprepare_filter_old = f"[{inputold}:a]aformat=s32p,atrim=start={inputold_start},asetpts=PTS-STARTPTS[a{output}s0p]"
    vprepare_filter_new = f"[{inputnew}:v]trim=end={length},setpts=PTS-STARTPTS[v{output}s1p]"
    aprepare_filter_new = f"[{inputnew}:a]aformat=s32p,atrim=end={length},asetpts=PTS-STARTPTS[a{output}s1p]"
    transition_filter = t_cross(f"{output}s0p", f"{output}s1p", length, type, output)
    return SEMICOLON.join([vprepare_filter_old, aprepare_filter_old, vprepare_filter_new, aprepare_filter_new, transition_filter])


### INTRO_OUT
def fade_out(input, start, length, output):
    vfade = f"[v{input}]fade=type=out:duration={length}[v{output}]"
    afade = f"[a{input}]afade=type=out:duration={length}:curve=tri[a{output}]"
    return SEMICOLON.join([vfade, afade])

def t_out(input, start, length, type, output):
    if type == "fade":
      return fade_out(input, start, length, output)
    eprint("t_out: something went wrong")

# SECTION
def seg_trans_out(input, start, length, type, output):
    vprepare_filter = f"[{input}:v]trim=start={start},setpts=PTS-STARTPTS[v{output}p]"
    aprepare_filter = f"[{input}:a]aformat=s32p,atrim=start={start},asetpts=PTS-STARTPTS[a{output}p]"
    transition_filter = t_out(f"{output}p", start, length, type, output)
    return SEMICOLON.join([vprepare_filter, aprepare_filter, transition_filter])


### CONCATENATE
def seg_concatenate(inputs, output):
    input_links = "".join([f"[v{i}][a{i}]" for i in inputs])
    return f"{input_links}concat=n={len(inputs)}:v=1:a=1[v{output}][a{output}]"

## Separate arguments into settings and steps.
## Settings start with double dash --likethis or --like=this or --like this
## -- Order does not matter and have a key, so settings are stored in a dict.
## Steps start with a single dash and can have parameters (not
##    values) and can be expressed -like this.
## -- Order is important so they are stored in a list.
def parse_args(argv):
    settings = dict()
    steps = list()
    curlist = list()

    for arg in argv[1:]:
        if arg == '--help' or arg == '-h':
            eprint_help()
            sys.exit(0)
        if arg[0] != '-':
            curlist.append(arg)
            continue
        if len(curlist) > 0:
            if curlist[0][1] == '-':
                settings[curlist[0][2:]] = curlist[1:].copy()
            else:
                steps.append(curlist.copy())
        del curlist[:]
        curlist.append(arg)
    else:
        if len(curlist) > 0:
            if curlist[0][1] == '-':
                settings[curlist[0][2:]] = curlist[1:].copy()
            else:
                steps.append(curlist.copy())
    return settings, steps

def calculate_segs_from_steps(steps):
    segs = list()
    file_n = -1
    n = 0
    for step in steps:
        if step[0] in ['-i', '-in']:
            seg_data = calculate_seg_in(n, steps, file_n, segs)
            segs.append(['in', seg_data])
        if step[0] in ['-o', '-out']:
            seg_data = calculate_seg_out(n, steps, file_n, segs)
            segs.append(['out', seg_data])
        if step[0] in ['-c', '-cross']:
            seg_data = calculate_seg_cross(n, steps, file_n, segs)
            segs.append(['cross', seg_data])
        if step[0] in ['-f', '-file']:
            file_n = file_n + 1
            seg_data = calculate_seg_inside(n, steps, file_n, segs)
            segs.append(['inside', seg_data])
        n = n + 1
    return segs

## CREATE FILTERS (THE ACTUAL FFMPEG FILTERS) FROM SEGS ##
def calculate_filters_from_segs(segs):
    filters = []
    for i in segs:
        if i[0] == "in":
            filters.append(seg_trans_in(i[1]['input'], i[1]['length'], i[1]['type'], i[1]['output']))
        if i[0] == "out":
            filters.append(seg_trans_out(i[1]['input'], i[1]['start'], i[1]['length'], i[1]['type'], i[1]['output']))
        if i[0] == "inside":
            filters.append(seg_inside(i[1]['input'], i[1]['start'], i[1]['length'], i[1]['output']))
        if i[0] == "cross":
            filters.append(seg_trans_cross(i[1]['inputold'], i[1]['inputnew'], i[1]['inputold_start'], i[1]['length'], i[1]['type'], i[1]['output']))

    inputs = [s[1]['output'] for s in segs]
    filters.append(seg_concatenate(inputs, "out"))

    return filters

## MAIN ##
settings, steps = parse_args(sys.argv)

## STEP AND SETTINGS VALIDATION ##
errors = list()
errors.extend(validate_steps(steps))
errors.extend(validate_settings(settings, steps))
for e in errors:
    eprint(e)
if len(errors) > 0:
    eprint("Please use the --help or -h options to get the syntax.")

## CALCULATE SEGS FROM STEPS (SEG IS SHORT FOR SEGMENT) ##
segs = calculate_segs_from_steps(steps)

## VALIDATE SEGS ##
errors = list()
errors.extend(validate_seg_order(segs))
for e in errors:
    eprint(e)
if len(errors) > 0:
    eprint("Steps are syntactically correct but something did not make sense.")

filters = calculate_filters_from_segs(segs)

print(SEMICOLON.join(filters))
